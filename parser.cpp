#include <iostream>
#include <fstream>
#include <string>

#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>

#define FN_NAME "namesdpt2018.csv"
#define FN_DPT  "dpt.csv"
#define FN_CRIM "faitsconstatespardepartement2011.csv"

#define SQL_SERVER "remotemysql.com"
/*#define SQL_USER "HAJaXVQlYw"
#define SQL_PWD "J3TcO2yMwF"
#define SQL_BASE "HAJaXVQlYw"
*/

#define SQL_USER "fOJzInghCN"
#define SQL_PWD "PnMJysRrF6"
#define SQL_BASE "fOJzInghCN"


#define CSV_DELIMITER ';'

#include <string>
#include <sstream>
#include <vector>


using namespace std;

void split(const string &s, char delim, vector<string> &elems) {
	stringstream ss(s);
	string item;
	while (getline(ss, item, delim)) {
		elems.push_back(item);
	}
}


vector<string> split(const string &s, char delim) {
	vector<string> elems;
	split(s, delim, elems);
	return elems;
}

bool isNumber(string line)
{
	return (atoi(line.c_str()));
} //fail for 0...

sql::Statement *sql_connect()
{
	sql::Driver *driver;
	sql::Connection *con;
	sql::Statement *stmt;

	/* Create a connection */
	driver = get_driver_instance();
	con = driver->connect(SQL_SERVER, SQL_USER, SQL_PWD);
	/* Connect to the MySQL test database */
	con->setSchema(SQL_BASE);
	stmt = con->createStatement();
	//delete con; // -> close the connection
	return stmt;
}

bool free_table(string table_name)
{
	try
	{
		sql::Statement *stmt = sql_connect();
		bool res = stmt->execute("DELETE FROM `"+table_name+"`");
		delete stmt;
		return res;
	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return false;
}

bool insert_dpt(sql::Statement* stmt, std::string line)
{
	bool res = false;
	try
	{
		auto args = split(line, '\t');
		if(args.size() > 2)
		{
			string id  = args[0];
			string lib = args[1];

			if(isNumber(id) && lib.length()>0)
			{
				//cout<<"INSERT INTO Departement ( "+id+" ,'"+lib+"')"<<endl;
				res = stmt->execute("INSERT INTO Departement VALUES ( "+id+" ,\""+lib+"\");");
			} else {
				std::cerr<<"Parsing failed for line: "<<line<<std::endl;
			}
		} else {
			std::cerr<<args.size()<<"Wrong format for line: "<<line<<std::endl;
		}

	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return res;
}

string insert_name(string line)
{
	string res = "";
	try
	{
		auto args = split(line, CSV_DELIMITER);
		if(args.size() == 5)
		{
			string sex  = args[0];
			string name = args[1];
			string year = args[2];
			string dpt  = args[3];
			string qtt  = args[4];

			if(dpt == "2A" || dpt == "2B") {// LA corse
				dpt = "20";
			}

			if(isNumber(sex) && isNumber(year) && isNumber(dpt) && isNumber(qtt) && name.length()>0 && sex.length()==1)
			{
				//cout<<"INSERT INTO Nom VALUES ('"+name+"','"+dpt+"','"+year+"','"+sex+"', '"+qtt+"')"<<endl;
				res = " (\""+name+"\",'"+dpt+"','"+year+"','"+sex+"', '"+qtt+"')";
			} else {
				std::cerr<<"Parsing failed for line: "<<line<<std::endl;
			}
		} else {
			std::cerr<<"Wrong format for line: "<<line<<std::endl;
		}
		return res;
	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return res;
}

void insert_names()
{
	std::cout<<"START parsing file "<<FN_NAME<<std::endl;

	std::ifstream csv_file(FN_NAME);
	sql::Statement *stmt = sql_connect();

	if(csv_file.is_open()){
		std::cout<<"Open file "<<FN_NAME<<std::endl;

		free_table("Nom");
		string line;
		string request = "INSERT INTO Nom VALUES ";
		int cptInsert = 0;
		while(getline(csv_file,line)){

			string val=insert_name(line);
			if(val.length()>2)
			{
				//cout<<val<<endl;
				cptInsert++;
				request += val;
			}

			if(cptInsert>5000)
			{
				cptInsert = 0;
				request+=";";
				//send request
				stmt->execute(request);

				//reset request
				request = "INSERT INTO Nom VALUES ";
			}
			else if(val.length()>2) {
				request+=" ,";
			}
		}
	}
	else{
		std::cerr<<"Cannot open file "<<FN_NAME<<std::endl;
	}

	try {

	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}

	cout << endl;

	csv_file.close();
	delete stmt;
}

void insert_dpts()
{
	std::cout<<"START parsing file "<<FN_DPT<<std::endl;

	std::ifstream csv_file(FN_DPT);
	sql::Statement *stmt = sql_connect();

	if(csv_file.is_open()){
		std::cout<<"Open file "<<FN_DPT<<std::endl;

		free_table("Departement");
		string line;
		while(getline(csv_file,line)){
			insert_dpt(stmt, line);
		}
	}
	else{
		std::cerr<<"Cannot open file "<<FN_DPT<<std::endl;
	}

	cout << endl;

	csv_file.close();
	delete stmt;
}

int getLastInsertId(sql::Statement* stmt)
{
	sql::ResultSet *res = stmt->executeQuery("SELECT LAST_INSERT_ID() AS id;");
	res->next();
	return res->getInt64("id");
}

bool insert_crime(sql::Statement* stmt, std::string line)
{
	bool res = false;
	try
	{
		auto args = split(line, ',');
		if(args.size() == 96)
		{
			string libelle = args[0];
			res = stmt->execute("INSERT INTO Crime (libelle) VALUES (\""+libelle+"\");");
			int idCrime = getLastInsertId(stmt);
			for(int i = 1; i<96; i++){
				std::string req = "INSERT INTO CrimeDpt VALUES ( ";
				req += std::to_string(i);
				req += " , ";
				req += std::to_string(idCrime);
				req += " , ";
				req += args[i];
				req += ");";
//std::cout<<req<<std::endl;
				res = stmt->execute(req);
			}
		} else {
			std::cerr<<args.size()<<"Wrong format for line: "<<line<<std::endl;
		}

	} catch (sql::SQLException &e) {
		cout << "# ERR: SQLException in " << __FILE__;
		cout << "(" << __FUNCTION__ << ") on line " << __LINE__ << endl;
		cout << "# ERR: " << e.what();
		cout << " (MySQL error code: " << e.getErrorCode();
		cout << ", SQLState: " << e.getSQLState() << " )" << endl;
	}
	return res;
}

void insert_crimes()
{
	std::cout<<"START parsing file "<<FN_CRIM<<std::endl;

	std::ifstream csv_file(FN_CRIM);
	sql::Statement *stmt = sql_connect();
	bool fl = true;

	if(csv_file.is_open()){
		std::cout<<"Open file "<<FN_CRIM<<std::endl;

		free_table("Crime");
		string line;
		while(getline(csv_file,line)){
			if(fl)
			fl = false;
			else
			insert_crime(stmt, line);
		}
	}
	else{
		std::cerr<<"Cannot open file "<<FN_DPT<<std::endl;
	}

	cout << endl;

	csv_file.close();
	delete stmt;
}


int main()
{
	/*free_table("Departement");
	insert_dpts();
	insert_names();*/
	free_table("Crime");
	free_table("CrimeDpt");
	insert_crimes();

}
